package game

import cg "gitlab.com/donachys/cannon-go/cannon-go"

//Player defines the behavior of a Game player.
type Player interface {
	PlayerName() string
	SetReady(bool)
	Ready() bool
	RegisterUpdateChannel(chan *cg.StreamingPlayResponse)
	UpdateChannel() chan<- *cg.StreamingPlayResponse
}

//CannonPlayer contains all of the data to represent a player in a Game.
type CannonPlayer struct {
	playerName string
	updates    chan *cg.StreamingPlayResponse
	ready      bool
}

//NewCannonPlayer creates a new CannonPlayer given a name string.
func NewCannonPlayer(name string) *CannonPlayer {
	return &CannonPlayer{playerName: name, updates: make(chan *cg.StreamingPlayResponse, 5)}
}

//SetReady sets the readiness value for the CannonPlayer.
func (cp *CannonPlayer) SetReady(ready bool) {
	cp.ready = ready
}

//Ready retrieves the readiness value for the CannonPlayer.
func (cp *CannonPlayer) Ready() bool {
	return cp.ready
}

//PlayerName retrieves the name of the player.
func (cp *CannonPlayer) PlayerName() string {
	return cp.playerName
}

//RegisterUpdateChannel registers a channel for sending updates with this player.
func (cp *CannonPlayer) RegisterUpdateChannel(updates chan *cg.StreamingPlayResponse) {
	cp.updates = updates
}

//UpdateChannel returns a channel for sending updates to this player.
func (cp *CannonPlayer) UpdateChannel() chan<- *cg.StreamingPlayResponse {
	return cp.updates
}
