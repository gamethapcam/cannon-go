package game

import (
	"errors"
	"fmt"
	"log"
	"math"
	"math/rand"
	"sync"
	"time"

	uuid "github.com/satori/go.uuid"
	cg "gitlab.com/donachys/cannon-go/cannon-go"
	"gitlab.com/donachys/cannon-go/util"
)

const (
	cooldownDuration = time.Second * 3
	minPlayers       = 2
	radconv          = math.Pi / 180.0
	g                = 9.8
)

//Game defines the expected behavior of a Game.
type Game interface {
	GameState(Player) cg.GameState
	AddPlayer(Player)
	AllReady() bool
	HandleRequest(cg.StreamingPlayRequest)
	GameID() string
	PlayerNum(Player) int
	SendUpdates()
	Cancel()
}

//CannonGame stores all of the information to represent a game.
type CannonGame struct {
	mu        sync.RWMutex
	Players   map[string]Player
	id        string
	state     cg.GameState_State
	field     *cg.Rect
	names     []string
	misses    [][]*cg.Point
	shots     map[int32]*cg.Shots
	bases     []*cg.Rect
	cannons   []*cg.Point
	outcomes  []cg.GameState_Outcome
	cooldowns []bool
}

//NewCannonGame creates a CannonGame.
func NewCannonGame() *CannonGame {
	g := new(CannonGame)
	g.id = util.Sha512Hash(uuid.NewV4().Bytes())
	g.Players = make(map[string]Player)
	g.shots = make(map[int32]*cg.Shots)
	g.state = cg.GameState_WAITING
	return g
}

//GameState constructs and returns a serializable GameState object for this game given a Player.
func (game *CannonGame) GameState(p Player) *cg.GameState {
	gs := new(cg.GameState)
	if p == nil || game == nil {
		return gs
	}
	pnum, err := game.PlayerNum(p)
	if err != nil {
		return gs
	}
	gs.State = game.state
	log.Println("gs.State", gs.State)
	gs.PlayerNum = pnum
	if gs.State == cg.GameState_COMPLETE && allOutcomesAre(game.outcomes, cg.GameState_CANCELED) {
		gs.Outcome = cg.GameState_CANCELED
		return gs
	}
	gs.Outcome = game.outcomes[pnum]
	gs.P0Misses = game.misses[0]
	gs.P1Misses = game.misses[1]
	gs.P0Base = game.bases[0]
	gs.P1Base = game.bases[1]
	gs.Cannon = game.cannons[pnum]
	return gs
}

func allOutcomesAre(outcomes []cg.GameState_Outcome, target cg.GameState_Outcome) bool {
	for _, outcome := range outcomes {
		if outcome != target {
			return false
		}
	}
	return true
}

//AddPlayer adds a player to this CannonGame.
func (game *CannonGame) AddPlayer(p Player) {
	game.Players[p.PlayerName()] = p
	game.names = append(game.names, p.PlayerName())
}

//AllReady returns true if all players are in a ready state.
func (game *CannonGame) AllReady() bool {
	game.mu.RLock()
	for _, v := range game.Players {
		if !v.Ready() {
			game.mu.RUnlock()
			return false
		}
	}
	game.mu.RUnlock()
	return true
}

//EnoughJoined returns true if enough players have joined.
func (game *CannonGame) EnoughJoined() bool {
	game.mu.RLock()
	enough := len(game.Players) >= minPlayers
	game.mu.RUnlock()
	return enough
}

//PlayerNum returns the player number of the given player in this CannonGame.
func (game *CannonGame) PlayerNum(p Player) (int32, error) {
	for i, n := range game.names {
		if p.PlayerName() == n {
			return int32(i), nil
		}
	}
	return -1, errors.New("player not found")
}

//GameID returns the unique ID of this CannonGame.
func (game *CannonGame) GameID() string {
	return game.id
}

//SendUpdates will send the current game state to all players.
func (game *CannonGame) SendUpdates() {
	// TODO: random ordering?
	game.mu.Lock()
	// Build up the shots fired update
	shotsFired := []*cg.Shots{}
	for _, shots := range game.shots {
		shotsFired = append(shotsFired, shots)
	}
	for _, player := range game.Players {
		resp := &cg.StreamingPlayResponse{
			GameState:  game.GameState(player),
			Msg:        fmt.Sprintf("update for %s in game %s", player.PlayerName(), game.GameID()),
			ShotsFired: &cg.ShotsFired{Shots: shotsFired},
		}
		player.UpdateChannel() <- resp
	}
	// Reset the shots fired to empty
	game.shots = make(map[int32]*cg.Shots)
	game.mu.Unlock()
}

//HandleRequest handles a request from a Player for this CannonGame.
func (game *CannonGame) HandleRequest(req cg.StreamingPlayRequest, playerKey string) {
	//Handle a ReadyAction
	if ra := req.GetRAction(); ra != nil {
		game.handleReadyAction(playerKey, ra)
	}
	if fa := req.GetFAction(); fa != nil {
		game.handleFireAction(playerKey, fa)
	}
}

//Cancel terminates the game.
func (game *CannonGame) Cancel() error {
	game.state = cg.GameState_COMPLETE
	for i := range game.outcomes {
		game.outcomes[i] = cg.GameState_CANCELED
	}
	game.SendUpdates()
	return nil
}

func (game *CannonGame) handleReadyAction(playerKey string, action *cg.ReadyAction) {
	game.Players[playerKey].SetReady(action.Ready)
	switch game.state {
	case cg.GameState_WAITING:
		if game.EnoughJoined() && game.AllReady() {
			game.start()
		}
	}
}

func (game *CannonGame) handleFireAction(playerKey string, action *cg.FireAction) {
	switch game.state {
	case cg.GameState_ACTIVE:
		pnum, err := game.PlayerNum(game.Players[playerKey])
		if err != nil {
			// TODO: player doesn't exist?
			return
		}
		if game.cooldowns[pnum] {
			log.Println("valid fire action for", playerKey)
			// convert rot and pitch from degrees to radians
			rot, pitch, pow := radconv*float64(action.Rotation), radconv*float64(action.Pitch), float64(action.Power)
			h := math.Pow(pow, 2) * math.Sin(2*pitch) / g // hypotenuse
			o := math.Sin(rot) * h                        // opposite side
			a := math.Cos(rot) * h                        // adjacent side
			shotLoc := cg.Point{
				X: game.cannons[pnum].X + int32(math.Floor(a)),
				Y: game.cannons[pnum].Y + int32(math.Floor(o)),
			}
			game.mu.Lock()
			game.findHit(shotLoc)
			game.findVictor()
			game.addShotFired(pnum, &shotLoc)
			game.mu.Unlock()
			// TODO: last n misses per player?
			game.misses[pnum] = append(game.misses[pnum], &shotLoc)
			// TODO: track cooldowns as time remaining for sending to client.
			game.cooldowns[pnum] = false
			time.AfterFunc(cooldownDuration, func() { game.cooldowns[pnum] = true })
			game.SendUpdates()
		}
	}
}

func (game *CannonGame) findVictor() {
	remainingPlayers := make([]Player, 0)
	for i, o := range game.outcomes {
		if o != cg.GameState_LOSS {
			remainingPlayers = append(remainingPlayers, game.Players[game.names[i]])
		}
	}
	if len(remainingPlayers) == 1 {
		pnum, err := game.PlayerNum(remainingPlayers[0])
		if err != nil {
			//player doesn't exist?
			return
		}
		game.outcomes[pnum] = cg.GameState_WIN
		game.state = cg.GameState_COMPLETE
	}
}

func (game *CannonGame) findHit(p cg.Point) {
	for pnum, c := range game.cannons {
		if c.X == p.X && c.Y == p.Y {
			game.outcomes[pnum] = cg.GameState_LOSS
		}
	}
}

func (game *CannonGame) addShotFired(pnum int32, p *cg.Point) {
	shots, ok := game.shots[pnum]
	if !ok {
		shots = &cg.Shots{}
		game.shots[pnum] = shots
		shots.PlayerNum = pnum
	}
	shots.Shots = append(shots.Shots, p)
}

func (game *CannonGame) start() {
	log.Println("Starting Game!", game.GameID())
	game.state = cg.GameState_ACTIVE
	game.field = &cg.Rect{
		TopLeft:  &cg.Point{X: 0, Y: 0},
		BotRight: &cg.Point{X: 100, Y: 100},
	}
	fieldWidth, fieldHeight := int32(88), int32(50)
	baseWidth, baseHeight := int32(5), int32(5)
	game.plantBases(fieldWidth, fieldHeight, baseWidth, baseHeight)
	game.plantCannons(baseWidth, baseHeight)
	for _, player := range game.Players {
		game.names = append(game.names, player.PlayerName())
		game.misses = append(game.misses, make([]*cg.Point, 0))
		game.outcomes = append(game.outcomes, cg.GameState_NONE)
		game.cooldowns = append(game.cooldowns, true)
	}
	game.SendUpdates()
}

func (game *CannonGame) plantBases(fieldWidth, fieldHeight, baseWidth, baseHeight int32) {
	game.bases = []*cg.Rect{
		&cg.Rect{
			TopLeft:  &cg.Point{X: 10, Y: 10},
			BotRight: &cg.Point{X: 20, Y: 20},
		},
		&cg.Rect{
			TopLeft:  &cg.Point{X: 60, Y: 40},
			BotRight: &cg.Point{X: 70, Y: 50},
		},
	}
	//divide field, randomly choose a horizontal or vertical split
	vertical := rand.Intn(1)
	minX, maxX := []int32{0, 0}, []int32{fieldWidth - 1, fieldWidth - 1}
	minY, maxY := []int32{0, 0}, []int32{fieldHeight - 1, fieldHeight - 1}
	switch vertical {
	case 0:
		half := fieldHeight / 2
		maxX[0], maxX[1] = maxX[0]-baseWidth, maxX[1]-baseWidth
		maxY[0], maxY[1] = half-baseHeight-1, maxY[1]-baseHeight
		minY[1] = half
	case 1:
		half := fieldWidth / 2

		maxX[0], maxX[1] = half-baseWidth-1, maxX[1]-baseWidth
		maxY[0], maxY[1] = maxY[0]-baseHeight, maxY[1]-baseHeight
		minX[1] = half
	}
	topLeftX := []int32{
		rand.Int31n(maxX[0]-minX[0]) + minX[0],
		rand.Int31n(maxX[1]-minX[1]) + minX[1],
	}
	topLeftY := []int32{
		rand.Int31n(maxY[0]-minY[0]) + minY[0],
		rand.Int31n(maxY[1]-minY[1]) + minY[1],
	}
	game.bases = []*cg.Rect{
		&cg.Rect{
			TopLeft:  &cg.Point{X: topLeftX[0], Y: topLeftY[0]},
			BotRight: &cg.Point{X: topLeftX[0] + baseWidth, Y: topLeftY[0] + baseHeight},
		},
		&cg.Rect{
			TopLeft:  &cg.Point{X: topLeftX[1], Y: topLeftY[1]},
			BotRight: &cg.Point{X: topLeftX[1] + baseWidth, Y: topLeftY[1] + baseHeight},
		},
	}
}

func (game *CannonGame) plantCannons(baseWidth, baseHeight int32) {
	cannonX := []int32{
		rand.Int31n(baseWidth),
		rand.Int31n(baseWidth),
	}
	cannonY := []int32{
		rand.Int31n(baseHeight),
		rand.Int31n(baseHeight),
	}
	game.cannons = []*cg.Point{
		&cg.Point{
			X: game.bases[0].TopLeft.X + cannonX[0],
			Y: game.bases[0].TopLeft.Y + cannonY[0],
		},
		&cg.Point{
			X: game.bases[1].TopLeft.X + cannonY[0],
			Y: game.bases[1].TopLeft.Y + cannonY[1],
		},
	}
}
