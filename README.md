## cannon-go

cannon-go is a multiplayer game using GRPC to simulate a cannon battle. This repository contains a terminal client. You can find a libgdx client at [cannon-gdx](https://gitlab.com/donachys/cannon-gdx).

#### build

To build the server and client binaries:
`make linux`
`make darwin`
`make windows`

To build a docker image for the server only:
`make docker`

#### game description

Inspired by the game gorillas and battleship, you are trying to hit your opponent's cannon. The catch is that you dont know exactly where it is, but it's somewhere within your opponent's castle walls. Modify the rotation, pitch, and power parameters to try and hit it!