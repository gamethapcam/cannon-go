package main

import (
	"math/rand"
	"time"

	cliclient "gitlab.com/donachys/cannon-go/cli-client"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	cliclient.Init()
	cliclient.RunApp()
	cliclient.Teardown()
}
