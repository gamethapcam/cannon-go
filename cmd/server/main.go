package main

import (
	"math/rand"
	"time"

	"gitlab.com/donachys/cannon-go/server"
)

func main() {
	rand.Seed(time.Now().UTC().UnixNano())
	server.Init()
}
