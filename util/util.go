package util

import (
	"crypto/sha512"
	"encoding/hex"

	tl "github.com/JoelOtter/termloop"
	cg "gitlab.com/donachys/cannon-go/cannon-go"
)

//Sha512Hash hashes a string.
func Sha512Hash(b []byte) string {
	h := sha512.New()
	h.Write(b)
	sha512Hash := hex.EncodeToString(h.Sum(nil))
	return sha512Hash
}

//MaxUint64 returns the max of uint64 a, b.
func MaxUint64(a, b uint64) uint64 {
	if a > b {
		return a
	}
	return b
}

//MaxFloat32 returns the max of float32 a, b.
func MaxFloat32(a, b float32) float32 {
	if a > b {
		return a
	}
	return b
}

//ClampFloat32 clamps the value a between lower and upper inclusive.
func ClampFloat32(lower, a, upper float32) float32 {
	if a < lower {
		return lower
	}
	if a > upper {
		return upper
	}
	return a
}

//MaxInt returns the max of Int a, b.
func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

//MinInt returns the min of Int a, b.
func MinInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

//SumInt sums and returns the ints in a.
func SumInt(a []int) (sum int) {
	for _, v := range a {
		sum += v
	}
	return
}

//TermLoopRect transforms a cannongo rect to a termloop rectangle.
func TermLoopRect(rect *cg.Rect, color tl.Attr) *tl.Rectangle {
	return tl.NewRectangle(
		int(rect.TopLeft.X), int(rect.TopLeft.Y),
		int(rect.BotRight.X-rect.TopLeft.X), int(rect.BotRight.Y-rect.TopLeft.Y),
		color,
	)
}
