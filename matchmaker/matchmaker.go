package matchmaker

import (
	"sync"

	"gitlab.com/donachys/cannon-go/game"
	"gitlab.com/donachys/cannon-go/gamestore"
)

var (
	cmm  *CannonMatchMaker
	once sync.Once
)

type MatchMaker interface {
	Match(*game.Player) game.CannonGame
	Clear()
}

type CannonMatchMaker struct {
	mutex sync.RWMutex
	queue []*game.CannonGame
}

//GetCannonMatchMaker returns the CannonMatchMaker singleton.
func GetCannonMatchMaker() *CannonMatchMaker {
	once.Do(func() {
		cmm = new(CannonMatchMaker)
	})
	return cmm
}

//Match will match and assign a player to a CannonGame and return it.
func (cmm *CannonMatchMaker) Match(p game.Player) *game.CannonGame {
	var g *game.CannonGame
	cmm.mutex.Lock()
	if len(cmm.queue) > 0 {
		g = cmm.queue[0]
		g.AddPlayer(p)
		cmm.queue = make([]*game.CannonGame, 0)
	} else {
		g = game.NewCannonGame()
		g.AddPlayer(p)
		cmm.queue = append(cmm.queue, g)
		gamestore.GetCannonGameStore().AddGame(g.GameID(), g)
	}
	cmm.mutex.Unlock()
	return g
}

//Clear removes all games from the MatchMaker queue.
func (cmm *CannonMatchMaker) Clear() {
	cmm.mutex.Lock()
	cmm.queue = make([]*game.CannonGame, 0)
	cmm.mutex.Unlock()
}
