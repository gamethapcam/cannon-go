package matchmaker

import (
	"testing"

	"gitlab.com/donachys/cannon-go/game"
)

func TestMatch(t *testing.T) {
	game1 := game.NewCannonGame()
	game1.AddPlayer(game.NewCannonPlayer("test_name"))

	game2 := game.NewCannonGame()
	game2.AddPlayer(game.NewCannonPlayer("test_name1"))
	game2.AddPlayer(game.NewCannonPlayer("test_name2"))
	cases := []struct {
		given []*game.CannonPlayer
		want  *game.CannonGame
	}{
		{
			given: []*game.CannonPlayer{game.NewCannonPlayer("test_name")},
			want:  game1,
		},
		{
			given: []*game.CannonPlayer{
				game.NewCannonPlayer("test_name1"),
				game.NewCannonPlayer("test_name2"),
			},
			want: game2,
		},
	}
	for _, c := range cases {
		cmm := GetCannonMatchMaker()
		cmm.Clear()
		var g *game.CannonGame
		for _, p := range c.given {
			g = cmm.Match(p)
		}
		for i := range c.want.Players {
			if g.Players[i].PlayerName() != c.want.Players[i].PlayerName() {
				t.Errorf("%v != %v", g.Players, c.want)
			}
		}
	}
}
