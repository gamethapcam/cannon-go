package gamestore

import (
	"sync"
	"time"

	cache "github.com/patrickmn/go-cache"
	"gitlab.com/donachys/cannon-go/game"
)

var (
	cgs  *CannonGameStore
	once sync.Once
)

//GameStore defines the behavior of a valid GameStore.
//Known implementors:
//  CannonGameStore
type GameStore interface {
	AddGame(string, *game.CannonGame)
	GetGame(string) (*game.CannonGame, bool)
	DelGame(string)
}

//CannonGameStore stores the games this server is aware of.
type CannonGameStore struct {
	games *cache.Cache
}

//GetCannonGameStore returns the CannonGameStore singleton.
func GetCannonGameStore() *CannonGameStore {
	once.Do(func() {
		cgs = new(CannonGameStore)
		cgs.init()
	})

	return cgs
}

func (cgs *CannonGameStore) init() {
	cgs.games = cache.New(2*time.Hour, 10*time.Minute)
}

//AddGame adds a CannonGame to the game store given a game id and CannonGame.
func (cgs *CannonGameStore) AddGame(id string, cg *game.CannonGame) {
	if cgs.games == nil {
		cgs.init()
	}
	cgs.games.Set(id, cg, cache.DefaultExpiration)
}

//GetGame retrieves and returns a CannonGame given a game id.
func (cgs *CannonGameStore) GetGame(id string) (*game.CannonGame, bool) {
	if cgs.games == nil {
		cgs.init()
	}
	item, found := cgs.games.Get(id)
	if found {
		match := item.(*game.CannonGame)
		return match, found
	}
	return nil, found
}

//DelGame deletes a game from the gamestore kiven a game id.
func (cgs *CannonGameStore) DelGame(id string) {
	if cgs.games == nil {
		cgs.init()
	}
	cgs.games.Delete(id)
}
