package cliclient

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"math/rand"
	"os"
	"strings"
	"sync"

	tl "github.com/JoelOtter/termloop"
	cg "gitlab.com/donachys/cannon-go/cannon-go"
	"gitlab.com/donachys/cannon-go/util"
	"google.golang.org/grpc/metadata"
)

var (
	ta        *TermApp
	once      sync.Once
	secret    string
	nameInput string
	client    cg.CannonGoClient
)

//GetTermApp retrieves the singleton TermApp.
func GetTermApp() *TermApp {
	once.Do(func() {
		//set up headers and open bi-directional stream
		header := metadata.New(map[string]string{"secret": secret, "user": nameInput})
		ctx := metadata.NewOutgoingContext(context.Background(), header)
		stream, err := client.StreamingPlay(ctx)
		if err != nil {
			log.Fatal("unable to intialize stream", err)
		}
		ta = newTermApp(stream)
	})
	return ta
}

//TermApp singleton represents the terminal game.
type TermApp struct {
	stream                 cg.CannonGo_StreamingPlayClient
	tlgame                 *tl.Game
	readyButton            *ClickableText
	aimSettings            *AimText
	rotation, pitch, power float32
	misses                 []*Misses //misses[0] are self misses
	cannon, p0base, p1base *tl.Rectangle
	lastState              *cg.GameState
}

//Misses are renderable.
type Misses struct {
	coords  []*cg.Point
	colorFG tl.Attr
	colorBG tl.Attr
}

//NewMisses creates a Misses struct and returns it.
func NewMisses(fg, bg tl.Attr) *Misses {
	return &Misses{coords: make([]*cg.Point, 0), colorFG: fg, colorBG: bg}
}

//Tick for implementing the tl.Drawable interface.
func (m *Misses) Tick(event tl.Event) {}

//Draw for rendering the misses.
func (m *Misses) Draw(s *tl.Screen) {
	for _, p := range m.coords {
		s.RenderCell(int(p.X), int(p.Y), &tl.Cell{Fg: m.colorFG, Bg: m.colorBG, Ch: 'x'})
	}
}

//AimText is for adjusting the aim settings with keyboard input.
type AimText struct {
	*tl.Text
}

//NewAimText creates a new AimText.
func NewAimText(x, y int, t string, colFG, colBG tl.Attr) *AimText {
	return &AimText{tl.NewText(x, y, t, colFG, colBG)}
}

//Tick for user input.
func (at *AimText) Tick(event tl.Event) {
	ta := GetTermApp()
	if ta.lastState.GetState() == cg.GameState_ACTIVE &&
		ta.lastState.GetOutcome() == cg.GameState_NONE &&
		event.Type == tl.EventKey {
		switch event.Key {
		case tl.KeyArrowRight:
			ta.rotation = float32(int(ta.rotation+1) % 360)
		case tl.KeyArrowLeft:
			ta.rotation = float32(int(ta.rotation+359) % 360)
		case tl.KeyArrowUp:
			ta.power = util.ClampFloat32(5, ta.power+1, 100)
		case tl.KeyArrowDown:
			ta.power = util.ClampFloat32(5, ta.power-1, 100)
		case tl.KeyCtrlP:
			ta.pitch = util.ClampFloat32(5, ta.pitch+1, 85)
		case tl.KeyCtrlO:
			ta.pitch = util.ClampFloat32(5, ta.pitch-1, 85)
		case tl.KeySpace:
			log.Printf("Sending fire\nrot: %v pitch: %v power: %v\n", ta.rotation, ta.pitch, ta.power)
			req := &cg.StreamingPlayRequest{
				Action: &cg.StreamingPlayRequest_FAction{
					FAction: &cg.FireAction{
						Rotation: ta.rotation,
						Pitch:    ta.pitch,
						Power:    ta.power,
					},
				},
			}
			if err := ta.stream.Send(req); err != nil {
				log.Fatalf("Failed to send a req: %v, err:%v", req, err)
			}
		}
		at.SetText(fmt.Sprintf("rot: %v pitch: %v pow: %v", ta.rotation, ta.pitch, ta.power))
	}
}

//Draw for rendering the outcome.
func (at *AimText) Draw(s *tl.Screen) {
	ta := GetTermApp()
	if ta.lastState.GetState() == cg.GameState_COMPLETE {
		at.SetText(fmt.Sprintf("Outcome: %v", ta.lastState.GetOutcome()))
	}
	at.Text.Draw(s)
}

//ClickableText is for the ready button.
type ClickableText struct {
	*tl.Text
}

//NewClickableText creates a new instance of ClickcableText.
func NewClickableText(x, y int, t string, colFG, colBG tl.Attr) *ClickableText {
	return &ClickableText{tl.NewText(x, y, t, colFG, colBG)}
}

//Tick for user input.
func (c *ClickableText) Tick(ev tl.Event) {
	x, y := c.Position()
	if ev.Type == tl.EventMouse && ev.Key == tl.MouseRelease && ev.MouseX == x && ev.MouseY == y {
		if fg, bg := c.Color(); bg == tl.ColorWhite {
			c.SetColor(fg, tl.ColorBlack)
		} else {
			c.SetColor(fg, tl.ColorWhite)
		}
		if c.Text.Text() == "unready" {
			req := &cg.StreamingPlayRequest{
				Action: &cg.StreamingPlayRequest_RAction{
					RAction: &cg.ReadyAction{
						Ready: true,
					},
				},
			}
			text := "ready"
			if err := ta.stream.Send(req); err != nil {
				log.Fatalf("Failed to send a req: %v, err:%v", req, err)
			}
			c.SetText(text)
		} else {
			req := &cg.StreamingPlayRequest{
				Action: &cg.StreamingPlayRequest_RAction{
					RAction: &cg.ReadyAction{
						Ready: false,
					},
				},
			}
			text := "unready"
			if err := ta.stream.Send(req); err != nil {
				log.Fatalf("Failed to send a req: %v, err:%v", req, err)
			}
			c.SetText(text)
		}
	}
}

//newTermApp builds and returns a pointer to a new TermApp struct.
func newTermApp(stream cg.CannonGo_StreamingPlayClient) *TermApp {
	ta := new(TermApp)
	ta.stream = stream
	ta.rotation = float32(int(rand.Float32()*36000) / 100.0)
	ta.pitch = float32(int(rand.Float32()*8500) / 100.0)
	ta.power = float32(int(rand.Float32()*5000) / 100.0)
	ta.tlgame = tl.NewGame()
	level := tl.NewBaseLevel(tl.Cell{
		Bg: tl.ColorGreen,
		Fg: tl.ColorBlack,
		Ch: 'ᚨ',
	})
	ta.tlgame.Screen().SetLevel(level)
	ta.readyButton = NewClickableText(1, 1, "unready", tl.ColorMagenta, tl.ColorBlack)
	ta.tlgame.Screen().SetFps(60)
	ta.tlgame.Screen().Level().AddEntity(ta.readyButton)
	ta.aimSettings = NewAimText(
		1, 2,
		fmt.Sprintf("rot: %v pitch: %v pow: %v", ta.rotation, ta.pitch, ta.power),
		tl.ColorMagenta, tl.ColorBlack,
	)
	ta.tlgame.Screen().Level().AddEntity(ta.aimSettings)
	ta.p0base = tl.NewRectangle(0, 0, 0, 0, tl.ColorWhite)
	ta.tlgame.Screen().Level().AddEntity(ta.p0base)
	ta.p1base = tl.NewRectangle(0, 0, 0, 0, tl.ColorWhite)
	ta.tlgame.Screen().Level().AddEntity(ta.p1base)
	ta.cannon = tl.NewRectangle(0, 0, 0, 0, tl.ColorRed)
	ta.tlgame.Screen().Level().AddEntity(ta.cannon)
	ta.misses = []*Misses{NewMisses(tl.ColorRed, tl.ColorBlue), NewMisses(tl.ColorRed, tl.ColorYellow)}
	for _, m := range ta.misses {
		ta.tlgame.Screen().Level().AddEntity(m)
	}
	return ta
}

//RunApp is the entry point for the App.
func RunApp() {
	client = cg.NewCannonGoClient(conn)
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter user name: ")
	rawNameInput, err := reader.ReadString('\n')
	if err != nil {
		log.Fatal("unable to read from stdin", err)
	}
	nameInput = strings.Trim(rawNameInput, "\n")
	resp, err := client.Join(context.Background(), &cg.JoinRequest{UserName: nameInput})
	if err != nil {
		log.Printf("argh, no joining")
	}
	secret = resp.GetSecret()
	log.Println("Secret:", secret)

	//set up the local gui
	ta := GetTermApp()
	castlesPlaced := false
	cannonPlaced := false

	waitChan := make(chan struct{})
	go func() {
		for {
			in, err := ta.stream.Recv()
			if err == io.EOF {
				close(waitChan)
				return
			}
			if err != nil {
				log.Fatal("failed.", err)
			}
			log.Printf("got response %v", in)
			log.Printf("got message %s", in.Msg)
			ta.lastState = in.GetGameState()
			if !castlesPlaced {
				p0 := ta.lastState.P0Base
				ta.p0base.SetPosition(int(p0.TopLeft.X), int(p0.TopLeft.Y))
				ta.p0base.SetSize(int(p0.BotRight.X-p0.TopLeft.X), int(p0.BotRight.Y-p0.TopLeft.Y))

				p1 := ta.lastState.P1Base
				ta.p1base.SetPosition(int(p1.TopLeft.X), int(p1.TopLeft.Y))
				ta.p1base.SetSize(int(p1.BotRight.X-p1.TopLeft.X), int(p1.BotRight.Y-p1.TopLeft.Y))
				castlesPlaced = true
			}
			if !cannonPlaced {
				cannonPoint := ta.lastState.Cannon
				ta.cannon.SetPosition(int(cannonPoint.X), int(cannonPoint.Y))
				ta.cannon.SetSize(1, 1)
				cannonPlaced = true
			}
			ta.misses[0].coords = ta.lastState.GetP0Misses()
			ta.misses[1].coords = ta.lastState.GetP1Misses()
		}
	}()
	ta.tlgame.Start()

	ta.stream.CloseSend()
	log.Println("exiting bidi stream")
	<-waitChan
}
