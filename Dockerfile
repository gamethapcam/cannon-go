FROM golang:1.11.2-stretch
ENV GO111MODULE=on
ENV REPO_NAME=gitlab.com/donachys/cannon-go

RUN mkdir -p /${REPO_NAME}
COPY . /${REPO_NAME}
WORKDIR /${REPO_NAME}
RUN make linux-server ; \
    mv /${REPO_NAME}/bin/linux/amd64/server / ; \
    rm -r /${REPO_NAME}

ENTRYPOINT ["/server"]
EXPOSE 55531
